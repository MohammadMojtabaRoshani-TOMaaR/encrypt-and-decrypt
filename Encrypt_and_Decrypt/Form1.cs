﻿using Encrypt_and_Decrypt.Encrypt_Decrypt;

using Encrypt_and_Decrypt.INterface;
using Encrypt_and_Decrypt.Mood;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Encrypt_and_Decrypt
{
    public partial class Form1 : Form
    {
        // private bool mood;
        Mood_cls m = new Mood_cls();
        public Form1()
        {
            InitializeComponent();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("First");
            comboBox1.Items.Add("Seccond");

           
        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {


            //Cryptoghraph_cls cryptoghraph_Cls = new Cryptoghraph_cls (iecrypter);
            if (m.moodFinder && comboBox1.Text == "First")
            {

                IEcrypter fiecrypter = new FirstMEncrypt();

                richTextBox1.Text = fiecrypter.Encryptor(txt_password.Text);
            }
            else if (m.moodFinder && comboBox1.Text == "Seccond")
            {

                IEcrypter siecrypter = new SeccondMEncrypt();

                richTextBox1.Text = siecrypter.Encryptor(txt_password.Text);
            }
            else if (m.moodFinder || comboBox1.Text == "First") {
                IDecrypter fdecrypter = new FirstMDecrypt();

                richTextBox1.Text = fdecrypter.Decryptor(txt_password.Text);

            }else {

                IDecrypter sdecrypter = new SeccondMDecrypt();

                richTextBox1.Text = sdecrypter.Decryptor(txt_password.Text);


            }
                
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void rb_encrypt_CheckedChanged(object sender, EventArgs e)
        {
            m.moodFinder = true;
           
        }

        private void rb_decrypt_CheckedChanged(object sender, EventArgs e)
        {
            m.moodFinder = false;
        }
    }
}
